import React, { useState } from "react";
import { constants, ICoords, IFigure } from "../constants";
import { randomFigure } from "../logic/tetris";

type Rotation = 0 | 1 | 2 | 3;
interface IContext {
  xPos: number;
  yPos: number;
  score: number;
  figure: IFigure;
  rotation: Rotation;
  blocks: IFigure[][];
  isPlaying: boolean;
  setCoords: (c: ICoords, r?: Rotation) => void;
  setState: (o: {}) => void;
}

interface IState {
  xPos: number;
  yPos: number;
  score: number;
  figure: IFigure;
  rotation: Rotation;
  blocks: IFigure[][];
  isPlaying: boolean;
  setCoords: (c: ICoords, r?: Rotation) => void;
  setState: (o: {}) => void;
}

const defaultContext: IContext = {
  xPos: constants.startingPoint().x,
  yPos: constants.startingPoint().y,
  score: 0,
  figure: randomFigure(),
  rotation: 0,
  blocks: [],
  isPlaying: true,
  setCoords: () => {},
  setState: () => {},
};

const Context = React.createContext(defaultContext);

export const ContextProvider = (props: any) => {
  const stateSetter = (o: {}) => setState({ ...state, ...o });
  const initState: IState = {
    ...defaultContext,
    setState: stateSetter,
  };
  const [state, setState] = useState(initState);

  return <Context.Provider value={state}>{props.children}</Context.Provider>;
};

export default Context;
export type { IState, Rotation };
