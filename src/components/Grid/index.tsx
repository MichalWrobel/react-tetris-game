import React, { useContext } from "react";
import Cell from "../Cell";
import Context, { IState } from "../../context/state";
import {
  getCoordsToDrop,
  isOccupied,
  pieceCoords,
  randomFigure,
  removeLines,
  rotate,
} from "../../logic/tetris";
import { Rotation } from "../../context/state";
import { constants } from "../../constants";
import { useInterval } from "./interval";
import "./style.sass";

interface IGrid {
  readonly size: { x: number; y: number };
}

const Grid = ({ size }: IGrid) => {
  const state = useContext(Context);
  const {
    xPos,
    yPos,
    setState,
    score,
    figure,
    rotation,
    blocks,
    isPlaying,
  } = state;

  useInterval(() => {
    if (isPlaying) {
      moveDown();
    }
  }, 350);

  const getCells = (rowIndex: number, count: number, color: string) => {
    return (
      <>
        {[...Array(count)].map((_, columnIndex) => {
          let c = "#3e3d3d";
          pieceCoords(figure, rotation, state.xPos, state.yPos).map((piece) => {
            if (piece.x === columnIndex && piece.y === rowIndex) {
              c = color;
            }
            return null;
          });

          if (blocks[columnIndex] && blocks[columnIndex][rowIndex]) {
            c = blocks[columnIndex][rowIndex].color;
          }
          return (
            <Cell
              key={`${columnIndex}-${rowIndex}`}
              coords={{ x: columnIndex, y: rowIndex }}
              color={c}
            />
          );
        })}
      </>
    );
  };

  const onKeyPressed = (e: any) => {
    if (!isPlaying) return;
    e.preventDefault();
    switch (e.keyCode) {
      case 37: // LEFT
        if (isOccupied(blocks, figure, state.xPos - 1, yPos, rotation)) return;
        setState({ ...state, xPos: state.xPos - 1 });
        break;
      case 39: // RIGHT
        if (isOccupied(blocks, figure, state.xPos + 1, yPos, rotation)) return;
        setState({ ...state, xPos: state.xPos + 1 });
        break;
      case 38: // UP
        if (
          isOccupied(blocks, figure, xPos, yPos, rotate(rotation) as Rotation)
        )
          return;
        setState({ ...state, rotation: rotate(rotation) as Rotation });
        break;
      case 40: // DOWN
        moveDown();
        break;
    }
  };

  const drop = () => {
    let scoreModifier = 1;
    const newFigure = randomFigure();
    const droppedBlocks = getCoordsToDrop(
      blocks,
      { x: xPos, y: yPos },
      figure,
      rotation
    );
    const areNewFigureCoordsOccupied = isOccupied(
      blocks,
      newFigure,
      constants.startingPoint().x,
      constants.startingPoint().y + 1,
      rotation
    );

    const linesRemoved = removeLines(blocks);
    if (linesRemoved && linesRemoved.length) {
      scoreModifier = linesRemoved.length;
    }

    // Stop if there is not enough space for the next block
    if (areNewFigureCoordsOccupied) {
      gameOver();

      return;
    }

    setState({
      ...state,
      xPos: constants.startingPoint().x,
      yPos: constants.startingPoint().y,
      rotation: 0,
      blocks: droppedBlocks,
      score: state.score + scoreModifier,
      figure: newFigure,
    });
  };

  const gameOver = () =>
    setState({
      ...state,
      isPlaying: false,
      score: state.score + 1,
    });

  const moveDown = () => {
    const isNextCoordOccupied: boolean = isOccupied(
      blocks,
      figure,
      xPos,
      yPos + 1,
      rotation
    );

    if (isNextCoordOccupied) {
      drop();
    } else {
      setState({ ...state, yPos: state.yPos + 1 });
    }
  };

  return (
    <div className="grid" tabIndex={0} onKeyDown={(e) => onKeyPressed(e)}>
      <div className="grid-over">
        {!isPlaying ? <span>Game over. Refresh to play again</span> : null}
      </div>
      {[...Array(size.y)].map((_, index) => (
        <div className="grid-row" key={index}>
          {getCells(index, size.x, figure.color)}
        </div>
      ))}
      <div className="grid-score">Score: {score}</div>
      <div className="grid-hint">
        (Click on the grid then use arProws to play)
      </div>
      <div className="grid-author">Tetris game created by Michał Wróbel</div>
    </div>
  );
};

export default Grid;
export type { IState };
