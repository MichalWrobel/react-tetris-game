import "./style.sass";

interface ICell {
  coords: { x: number; y: number };
  color?: string;
}

const Cell = ({ coords, color }: ICell) => (
  <span
    className="cell"
    key={`${coords.x}-${coords.y}`}
    style={{ backgroundColor: `${color}` }}
  ></span>
);

export default Cell;
