import { constants, IFigure, ICoords } from "../constants";
import { Rotation } from "../context/state";

const iterateBlocks = (
  type: IFigure,
  x: number,
  y: number,
  direction: number,
  callback: any
): void => {
  let bit: number,
    row: number = 0,
    col: number = 0,
    blocks: number = type.blocks[direction];
  for (bit = 0x8000; bit > 0; bit = bit >> 1) {
    if (blocks & bit) {
      callback(x + col, y + row);
    }
    if (++col === 4) {
      col = 0;
      ++row;
    }
  }
};

const isOccupied = (
  blocks: IFigure[][],
  type: IFigure,
  xOcc: number,
  yOcc: number,
  direction: number
): boolean => {
  let result = false;
  const { x, y } = constants.board;
  iterateBlocks(type, xOcc, yOcc, direction, (xPos: number, yPos: number) => {
    if (
      xPos < 0 ||
      xPos >= x ||
      yPos < 0 ||
      yPos >= y ||
      getBlock(blocks, xPos, yPos)
    )
      result = true;
  });
  return result;
};

const getFigures = (
  blocksFromState: IFigure[][],
  coords: ICoords,
  figure: IFigure
) => {
  let newBlocks = blocksFromState;
  newBlocks[coords.x] = newBlocks[coords.x] || [];
  newBlocks[coords.x][coords.y] = figure;

  return newBlocks;
};

const getCoordsToDrop = (
  blocksFromState: IFigure[][],
  coords: ICoords,
  figure: IFigure,
  stateRotation: Rotation
) => {
  let figs: IFigure[][] = [];
  iterateBlocks(
    figure,
    coords.x,
    coords.y,
    stateRotation,
    (x: number, y: number) => {
      figs = getFigures(blocksFromState, { x, y }, figure);
    }
  );

  return figs;
};

const getBlock = (blocks: IFigure[][], x: number, y: number): IFigure | null =>
  blocks && blocks[x] ? blocks[x][y] : null;

const randomFigure = (): IFigure => {
  const { i, j, l, o, s, t, z } = constants.figures;
  const figures = [
    i,
    i,
    i,
    i,
    j,
    j,
    j,
    j,
    l,
    l,
    l,
    l,
    o,
    o,
    o,
    o,
    s,
    s,
    s,
    s,
    t,
    t,
    t,
    t,
    z,
    z,
    z,
    z,
  ];

  return figures.splice(0 + Math.random() * (figures.length - 1 - 0), 1)[0];
};

const pieceCoords = (
  figure: IFigure,
  rotation: number,
  x: number,
  y: number
): ICoords[] => {
  const coords: ICoords[] = [];
  iterateBlocks(figure, x, y, rotation, (ix: number, iy: number) => {
    coords.push({ x: ix, y: iy });
  });

  return coords;
};

const rotate = (rotation: number): number => {
  if (rotation === 3) return 0;
  return ++rotation;
};

const removeLines = (blocks: IFigure[][]) => {
  let x,
    y,
    complete,
    result;
  for (y = constants.board.y; y > 0; --y) {
    complete = true;
    for (x = 0; x < constants.board.x; ++x) {
      if (!getBlock(blocks, x, y)) complete = false;
    }
    if (complete) {
      result = removeLine(blocks, y);
      y = y + 1;
    }
  }

  return result;
};

const removeLine = (blocks: IFigure[][], n: number) => {
  let x, y;
  let figs: IFigure[][] = [];
  for (y = n; y >= 0; --y) {
    for (x = 0; x < constants.board.x; ++x)
      figs = getFigures(
        blocks,
        { x, y },
        y === 0 ? null : (getBlock(blocks, x, y - 1) as any)
      );
  }
  return figs;
};

export {
  removeLines,
  getCoordsToDrop,
  rotate,
  pieceCoords,
  iterateBlocks,
  isOccupied,
  randomFigure,
};
