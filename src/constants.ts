interface ICoords {
  x: number;
  y: number;
}

interface IConstants {
  readonly board: ICoords;
  readonly figures: {
    i: IFigure;
    j: IFigure;
    l: IFigure;
    o: IFigure;
    s: IFigure;
    t: IFigure;
    z: IFigure;
  };
  readonly startingPoint: () => ICoords;
}

/*
  COLORS
    CYAN    #00fff6
    GREEN   #00ff37
    ORANGE  #ff9d00
    PINK    #ff00e5
    RED     #ff0000
    WHITE   #ffffff
    YELLOW  #ffff00
*/
type Color =
  | "#00fff6"
  | "#00ff37"
  | "#ff9d00"
  | "#ff00e5"
  | "#ff0000"
  | "#ffffff"
  | "#ffff00";

interface IFigure {
  size: number;
  blocks: number[];
  color: Color;
}

export const constants: IConstants = {
  board: { x: 11, y: 19 },
  figures: {
    i: { blocks: [0x0f00, 0x2222, 0x00f0, 0x4444], color: "#00fff6", size: 4 }, // CYAN
    j: { blocks: [0x44c0, 0x8e00, 0x6440, 0x0e20], color: "#00ff37", size: 3 }, // GREEN
    l: { blocks: [0x4460, 0x0e80, 0xc440, 0x2e00], color: "#ff9d00", size: 3 }, // ORANGE
    o: { blocks: [0xcc00, 0xcc00, 0xcc00, 0xcc00], color: "#ff00e5", size: 2 }, // PINK
    s: { blocks: [0x06c0, 0x8c40, 0x6c00, 0x4620], color: "#ff0000", size: 3 }, // RED
    t: { blocks: [0x0e40, 0x4c40, 0x4e00, 0x4640], color: "#ffffff", size: 3 }, // WHITE
    z: { blocks: [0x0c60, 0x4c80, 0xc600, 0x2640], color: "#ffff00", size: 3 }, // YELLOW
  },
  startingPoint: function (): ICoords {
    return { x: Math.floor(this.board.x / 2) - 1, y: -1 };
  },
};

export type { IFigure, ICoords };
