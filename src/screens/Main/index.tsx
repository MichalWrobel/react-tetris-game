import React from "react";
import Grid from "../../components/Grid/index";
import { constants } from "../../constants";

const Main = () => (
  <div>
    <Grid size={{ x: constants.board.x, y: constants.board.y }} />
  </div>
);

export default Main;
