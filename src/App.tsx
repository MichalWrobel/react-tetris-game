import React from "react";
import Main from "./screens/Main/index";
import { ContextProvider } from "./context/state";

const App = () => {
  return (
    <ContextProvider>
      <Main />
    </ContextProvider>
  );
};

export default App;
